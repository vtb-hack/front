import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import LoginPage from '../page/login.page.vue'
import PayPage from "@/page/pay.page.vue";
import HomePage from "@/page/home.page.vue";
import ErrorPage from "@/page/error.page.vue";
import PaymentInfoPage from "@/page/payment-info.page.vue";
import ConfirmPage from "@/page/confirm.page.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/login',
        name: 'Login',
        component: LoginPage
    },
    {
        path: '/confirm/:login',
        name: 'confirm',
        component: ConfirmPage
    },
    {
        path: '/',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/pay',
        name: 'Pay',
        component: PayPage
    },
    {
        path: '/payment-info/:id',
        name: 'Payment info',
        component: PaymentInfoPage
    },
    {
        path: '/error/:status',
        name: 'Error',
        component: ErrorPage
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
