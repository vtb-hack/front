import Vue from "vue";
import Vuex from "vuex";
import CurrentUserModule from "./current.user.store";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
    currentUserModule: CurrentUserModule,
    }
});
