export const ACTION_LOGIN = 'ACTION_LOGIN';
export const ACTION_LOGOUT = 'ACTION_LOGOUT';


export const SET_CURRENT_USER = 'SET_CURRENT_USER';


export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const GET_CURRENT_USER_IS_STUDENT = 'GET_CURRENT_USER_IS_STUDENT';
export const GET_CURRENT_USER_IS_TEACHER = 'GET_CURRENT_USER_IS_TEACHER';


const module = {
    state: {
        currentUser: null
    },
    getters: {
        [GET_CURRENT_USER]: (state: any) => {
            return state.currentUser !== null ? {...state.currentUser} : null;
        },
        [GET_CURRENT_USER_IS_STUDENT]: (state: any) => {
            return state.currentUser !== null && state.currentUser.role === 'ROLE_STUDENT';
        },
        [GET_CURRENT_USER_IS_TEACHER]: (state: any) => {
            return state.currentUser !== null && state.currentUser.role === 'ROLE_TEACHER';
        }
    },
    mutations: {
        [SET_CURRENT_USER]: (state: any, user: any) => {
            return state.currentUser = user;
        }
    },
    actions: {

        [ACTION_LOGIN]: (action: any, user: any) => {
            action.commit(SET_CURRENT_USER, user);
        },

        [ACTION_LOGOUT]: (action: any) => {
            action.commit(SET_CURRENT_USER, null);
        }

    }

}

export default module;