export class TokenRepository {

    public static setToken(token: string): void {
        // const result: string = (typeof token === 'string') ? token : JSON.stringify(token);
        localStorage.setItem('token', token);
    }

    public static getToken(): string | null {
        return localStorage.getItem('token');
        // const token = localStorage.getItem('token');
        // if(!token) return null;
        // return JSON.parse(token);
    }

    public static removeToken(): void {
        localStorage.removeItem('token');
    }

}