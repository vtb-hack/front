import {PaymentModel} from "@/model/payment.model";
import axios, {AxiosResponse} from "axios";

export class PaymentService {

    private static BASE_PATH: string = '/api/payments';

    public static payment(paymentModel: PaymentModel): Promise<AxiosResponse<number>> {
        return axios.post(this.BASE_PATH, paymentModel);
    }

    public static getPayment(id: number): Promise<AxiosResponse<PaymentModel>> {
        return axios.get(this.BASE_PATH + '/' + id);
    }

}