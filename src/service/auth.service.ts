import axios, {AxiosResponse} from "axios";

export class AuthService {

    private static BASE_PATH: string = '/api/auth';

    public static login(login: string, password: string): Promise<AxiosResponse<string>> {
        return axios.post(`${this.BASE_PATH}/login`, {
            login: login,
            password: password
        })
    }

    public static changePasswordWithConfirmSms(confirm: string, login: string, newPassword: string): Promise<AxiosResponse<void>> {
        return axios.post(`${this.BASE_PATH}/confirm-sms/change-password`, {
            confirm: confirm,
            login: login,
            newPassword: newPassword
        })
    }

}