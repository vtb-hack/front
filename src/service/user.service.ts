import axios, {AxiosResponse} from "axios";
import UserModel from "@/model/user.model";

export class UserService {

    private static BASE_PATH: string = '/api/users';

    public static getCurrent(): Promise<AxiosResponse<UserModel>> {
        return axios.get(`${this.BASE_PATH}/current`);
    }

}