import axios, {AxiosResponse} from "axios";

export class CursorLogService {

    private static BASE_PATH: string = '/api/cursor-log';

    public static logBatch(events: {x: number, y: number, timestamp: number}[]): Promise<AxiosResponse<void>> {
        return axios.post(this.BASE_PATH + '/batch', {
            batch: events
        })
    }

}