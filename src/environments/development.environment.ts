import {EnvironmentModel} from "@/model/environment.model";

export const DevelopmentEnvironment: EnvironmentModel = {
    baseURL: 'http://localhost:8080',
}