import {EnvironmentModel} from "@/model/environment.model";

export const ProductionEnvironment: EnvironmentModel = {
    baseURL: 'http://188.119.113.107:8080',
}
