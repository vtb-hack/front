import {DevelopmentEnvironment} from './development.environment';
import {Dictionary} from "vue-router/types/router";
import {EnvironmentModel} from "@/model/environment.model";
import {ProductionEnvironment} from "@/environments/production.environment";

const env: Dictionary<EnvironmentModel> = {
    development: DevelopmentEnvironment,
    production: ProductionEnvironment
}

export const environment = env[process.env.NODE_ENV];
