import axios from "axios";
import {TokenRepository} from '../repository/token.repository';
import router from "../router";
import store from '../store'
import {environment} from "../environments/environment";
import {ACTION_LOGOUT} from "../store/current.user.store";

axios.interceptors.request.use((r) => {
    if(TokenRepository.getToken() !== null) {
        r.headers['Authorization'] = 'Bearer ' + TokenRepository.getToken();
    }
    return r;
});

axios.interceptors.response.use(
    r => r,
    e => {
        const resErr = e.response;
        const isErrorPage = /^\/error\/\d+$/.test(router.currentRoute.fullPath);

        if(isErrorPage) return Promise.reject(e);

        if(resErr && resErr.status === 401) {
            delete axios.defaults.headers['Authorization'];
            TokenRepository.removeToken();
            router.push('/login');
            store.dispatch(ACTION_LOGOUT);
        }
        else if(resErr && resErr.status === 403) {
            router.push('/error/403');
        }
            // else if(resErr.status === 401 && TokenRepository.getToken()) {
            //     delete axios.defaults.headers['Authorization'];
            //     return AuthService.refreshToken(TokenRepository.getToken().refresh_token)
            //         .then(res => {
            //             TokenRepository.setToken(res.data);
            //             resErr.config.headers['Authorization'] = 'Bearer ' + TokenRepository.getToken().access_token;
            //             return axios.request(resErr.config);
            //         })
            //         .catch(err => {
            //             TokenRepository.removeToken();
            //             Vue.$router.push('/login');
            //         });
        // }
        else {
            return Promise.reject(e);
        }
    }
);

axios.defaults.baseURL = environment.baseURL;

window.axios = {...axios};