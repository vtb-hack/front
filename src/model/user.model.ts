export default class UserModel {
    login!: string;
    firstName!: string;
    lastName!: string;
    email!: string;
    role!: string;
}