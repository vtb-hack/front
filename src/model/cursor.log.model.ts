export class CursorLogModel {
    timestamp!: number;
    button!: string;
    state!: string;
    x!: number;
    y!: number;
}